package palindrome;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class contains few unit tests that validate the 
 * functionality of the methods of the Palindrome class.
 * @Author  Ashish Bhagat
 * @Version 1.0
 */
public class PalindromeTest {

	/**
	 * Validates the working of the rotateInput() method.
	 */
    @Test
    public void testRotateInput() {
		String errorMessage = "Rotation failing!!!";
		Palindrome palindrome = new Palindrome();

		// assign a string
		palindrome.setInput("aaaad");

		// perform tests
		palindrome.rotateInput();
		assertEquals(errorMessage,"aaada", palindrome.getInput());
		palindrome.rotateInput();
		assertEquals(errorMessage,"aadaa", palindrome.getInput());
		palindrome.rotateInput();
		assertEquals(errorMessage, "adaaa", palindrome.getInput());
		palindrome.rotateInput();
		assertEquals(errorMessage, "daaaa", palindrome.getInput());

		// assign a string
		palindrome.setInput("abcd");

		// perform tests
		palindrome.rotateInput();
		assertEquals(errorMessage,"bcda", palindrome.getInput());
		palindrome.rotateInput();
		assertEquals(errorMessage,"cdab", palindrome.getInput());
		palindrome.rotateInput();
		assertEquals(errorMessage, "dabc", palindrome.getInput());
    }

	/**
	 * Validates the working of the testIsInputPalindrome() method.
	 */
    @Test
    public void testIsInputPalindrome() {
		Palindrome palindrome = new Palindrome();

		palindrome.setInput("aaaad");
		// assertTrue(palindrome.permutateInput());
		assertEquals(true, palindrome.permutateInput());

		palindrome.setInput("abcd");
		//assertTrue(palindrome.permutateInput());
		assertEquals(false, palindrome.permutateInput());
    }
}