package palindrome;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * This program takes a user input and tests whether it a rotational
 * palindrome.
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class App {
    private static final Logger logger = Logger.getLogger( App.class.getName() );

    public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		logger.log(Level.INFO, "Enter a string: ");

		String input = new String();
		input = reader.nextLine();

		Palindrome palindrome = new Palindrome();
		palindrome.setInput(input);

		if(palindrome.permutateInput()) {
			logger.log(Level.INFO, input + " is a rotational palindrome!");
		} else {
			logger.log(Level.INFO, input + " is NOT a rotational palindrome!");
		}
    }
}
