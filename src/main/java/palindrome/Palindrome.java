package palindrome;

/**
 * This class consists of methods to test if the different
 * permutations of a string is a palindrome.
 * @author  Ashish Bhagat
   @version 1.0
 */
public class Palindrome {
    private String input;
    public int offset = 1; // since shifting one char at a time

	/**
	 * Return the user input.
	 * @return String 
	 */
    public String getInput() {
		return input;
    }

	/**
	 * Initiates the instance variable of the class to user input.
	 * @param  input The user input.
	 */
    public void setInput(String input) {
		this.input = input;
    }

	/**
	 * Tests if the different permutations of a string is a palindrome.
	 * @return boolean True if user input is a palindrome, false otherwise
	 */
    public boolean permutateInput() {
		for (int i=0; i < input.length(); i++) {
			if(isInputPalindrome()) {
				return true;
			} else {
				rotateInput();
			}
		}
		return false;
    }

	/**
	 * Shifts every character in the string to the left by one unit.
	 */
    public void rotateInput() {
		input =  input.substring(offset) + input.substring(0, offset);
    }

	/**
	 * Checks if the user input is a palindrome.
	 *
	 * @return boolean True if user input is a palindrome, false otherwise.
	 */
    public boolean isInputPalindrome() {
		for(int j=0, k=input.length() - 1; k > j; j++, k--) {
			if (input.charAt(j) != input.charAt(k)) {
				return false;
			}
		}
		return true;
    }
}